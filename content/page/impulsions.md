## Covid-19 - Pour du libre et de l'open en conscience (quelles mesures aujourd'hui et demain)

La crise actuelle démontre la fragilité du système économique et organisationnel sur lequel reposent nos sociétés.
> Si elle en démontre la nécessité, il ne faut pas penser qu'une telle crise suffira à opérer un basculement. Penser un « après » nécessite donc un long travail de déconstruction opéré d'ores et déjà par les communautés constituées autour du libre, de l'*open* et des communs numériques, qui démontrent par leur existence même et leurs actions concrètes la possibilité de construire des projets d'intérêt général autrement.

Écrit durant la crise, le [mémorandum](/page/memo) rappelle l'importance et la place des initiatives ouvertes, participatives et collaboratives pour notre société. Il permettra de se souvenir, mais aussi d'inspirer nos politiques publiques à venir. Devant l'urgence, il se double d'un certain nombre de mesures immédiatement envisageables et parfois déjà initiées.

### L'on tire de cette situation les enseignements suivants :

1. Les mouvements « libres et ouverts », et ceux ancrés dans une dynamique de « communs numériques » participent aujourd'hui rapidement et justement à répondre aux besoins révélés quotidiennement par la crise sanitaire et sociale.

2. La croissance du nombre de « communs numériques » permet d'imaginer une société structurée non pas autour de la détention d'une technologie, mais au contraire autour de la capacité des acteurs à travailler ensemble et à créer de la valeur en commun.

3. L'attrait fort des modèles collaboratifs ouverts se retrouve confronté à une acculturation et une éducation encore insuffisante des acteurs souhaitant collaborer.

4. Le développement d'une économie pérenne repose sur une sensibilisation aux enjeux juridiques et socio-économiques sous-jacents, afin d'assurer que ces nouveaux équilibres s'appuient sur des règles suffisamment consensuelles, claires et comprises.

5. Les technologies numériques essentielles doivent être des « communs numériques » bâtis par des communautés respectueuses de la liberté et de leurs droits fondamentaux. Cette solution est plus longue, mais atteignable par nos sociétés si elles en prennent la mesure.

6. Plus l’environnement, le contexte, les sujets montent en complexité, plus il devient essentiel de donner de nouvelles capacités d’actions et des moyens aux communautés distribuées, de se doter ainsi de nouveaux processus collectifs pour débattre, choisir et agir.


> Compte tenu de la situation de crise, il est urgent et nécessaire dès maintenant que, par principe, des financements publics soient intégrés aux appels à projets ou appels à manifestation d'intérêt (AMI) de toutes les agences publiques et convergent pour alimenter un fonds de communs ouverts et pérennes.

### Mesures actionnables

Plus encore, à l'aune de la crise que nous vivons toutes et tous et pour ne pas subir des mesures biaisées par une récupération politique ou économique des événements, nous souhaitons être assuré.e.s que d'ici 2021  :

1. L'intérêt des dynamiques libres, ouvertes et collaboratives soit officiellement reconnu et soutenu  dans leurs formes technologiques (logiciels, bases de données, documentations et spécifications, etc.) et systémiques (recherche ouverte, gouvernance ouverte, culture ouverte, etc.), en pensant la création et l'entretien d'infrastructures numériques essentielles à ces projets d'intérêt général.

2. Des [partenariats publics-communs](http://blogfr.p2pfoundation.net/2017/06/09/vers-partenariats-public-communs/) soient réellement ouverts, dans chaque région, accompagnés de dispositifs nationaux, favorisant l'émergence de communs dans les champs numériques de notre société, dans des dynamiques d'ouverture, de collaboration et de frugalité numérique.

3. Les modalités économiques et fiscales à destination des acteurs du numérique soient repensées pour éviter à tout prix les enclosures numériques et encourager la valorisation publique au travers de la diffusion ouverte des ressources financées par de l'argent public.

4. Les modèles de propriété – et encore plus de « propriété intellectuelle » – soient interrogés véritablement à l'aune d'une société soutenable qui soit durable et prospère. Cela au moins dans leurs usages et capacités à répondre aux préoccupations sociétales majeures, en temps de crise et en temps normal.

5. Un accompagnement soit mis en place vers l'ouverture des contenus et infrastructures des organisations publiques ayant pour mission l'accès ou la diffusion de la culture et des connaissances.

6. Les synergies entre l'ESS et les communs soient renforcées dans le monde du numérique et des communs de la connaissance, en construisant des modèles d'économie sociale dans le numérique et en s'appuyant sur l'approche par les communs pour défendre les principes d'intérêt général et d'utilité sociale.

7. Le statut des personnes contributrices aux communs soit reconnu en terme social et sociétal en leur consacrant des droits effectifs et opposables.
