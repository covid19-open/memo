## Soutien

Le texte qui suit a été nourri de la contribution d'une trentaine de personnes œuvrant aujourd'hui en se fondant sur les principes communs de la libre circulation de l'information, de gouvernance ouverte et de modèles sociaux durables.

Plusieurs organisations ont traduit leur soutien par la publication du mémorandum et/ou des enseignements et mesures sur leur site.

Liste des organisations :
- [ADOA](http://adoa.solutions/)
- [ADULLACT](https://adullact.org/)
- [API : Action of Public Interest](https://apifr.org/) - ([lien](https://apifr.org/#actu))
- [Ça reste ouvert](https://caresteouvert.fr) - ([lien](https://blog.caresteouvert.fr/covid-19-pour-du-libre-et-de-lopen-en-conscience/))
- Cap sur les Communs
- [CNLL (Union des entreprises du logiciel libre)](https://cnll.fr/) - ([lien](https://cnll.fr/news/le-cnll-soutien-le-m%C3%A9morandum-covid-19-pour-du-libre-et-de-lopen-en-conscience/))
- [Code for France](https://codefor.fr/) - ([lien](https://codefor.fr/assemblies/codeforfr/f/47/posts/16))
- [Coexiscience](http://www.coexiscience.fr/) - ([lien](http://www.coexiscience.fr/coexiscience-introduisant-ce-memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience/))
- [Covid-initiatives.org](https://covid-initiatives.org/)
- [DRISS](https://driss.org/)
- [Faire Ecole Ensemble (Fée)](https://wiki.faire-ecole.org/wiki/Association_Faire_Ecole_Ensemble) - ([lien](https://wiki.faire-ecole.org/wiki/M%C3%A9morandum_Covid-19_pour_du_libre_et_de_l%27open_en_conscience_:_enseignement_et_impulsions_futures#Mesures_actionnables))
- [Fédéation Open Space Makers](https://www.federation-openspacemakers.com) - ([lien](https://www.federation-openspacemakers.com/fr/communautes/actualites/covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions-futures/))
- [Framasoft](https://framasoft.org/fr/) - ([lien](https://framablog.org/2020/04/29/memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions-futures/))
- [HackYourResearch](https://hyr.science) - ([lien](https://hyr.science/2020/04/memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions-futures/))
- [Inno³](https://inno3.fr/) - ([lien](https://inno3.fr/actualite/memorandum-covid-19-pour-du-libre-et-de-lopen-en-conscience-enseignements-et-impulsions))
- [La Coop des Communs](https://coopdescommuns.org/fr/association/) - ([lien](https://coopdescommuns.org/fr/soutien-covid-19-pour-du-libre-et-de-lopen-en-conscience/))
- [La Fabrique des Mobilités](http://lafabriquedesmobilites.fr/)
- [La Mouette, association francophone de promotion et de défense de la bureautique libre](https://www.lamouette.org) - ([lien](https://www.lamouette.org/index.php/communiques/90-covid-19-pour-du-libre-et-de-l-open-en-conscience    ))
- [La MYNE](https://www.lamyne.org/) - ([lien](https://pad.lamyne.org/s/memorandum-pour-du-libre-et-de-l-open-en-conscience#))
- LibreAccès
- [Montpel'libre](https://montpellibre.fr) - ([lien](https://montpellibre.fr/spip.php?article4807))
- [NAOS](http://naos-cluster.com/) -([lien](https://naos-cluster.com/2020/04/29/lettre-ouverte-pour-du-libre-et-de-lopen-en-conscience/))
- [OpenDataFrance](http://www.opendatafrance.net/) - ([lien](http://www.opendatafrance.net/2020/04/30/covid-19-pour-du-libre-et-de-lopen-en-conscience/))
- [OpenStreetMap France](https://www.openstreetmap.fr/) - ([lien](https://www.openstreetmap.fr/memorandum-pour-du-libre-et-de-lopen-en-conscience/))
- [Sharelex](https://sharelex.org/) - ([lien](https://sharelex.org/t/memorandum-covid-19-pour-du-libre-et-de-l-open-en-conscience-enseignements-et-impulsions-futures/938))
- [Veni, Vidi, Libri](https://vvlibri.org/fr/blog/memorandum2020) -([lien](https://vvlibri.org/fr/blog/memorandum2020))
- [Wikimedia France](https://www.wikimedia.fr) - ([lien](https://www.wikimedia.fr/covid-19-pour-du-libre-et-de-lopen-en-conscience/))
- [Worteks](https://www.worteks.com/fr/) - ([lien](https://www.worteks.com/fr/2020/05/18/covid-19-pour-du-libre-et-de-lopen-en-conscience/))

Pour contacter les initiatives participantes : covid19-open-memo@framalistes.org
