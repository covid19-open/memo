## Ressources

L'actualité étant particulièrement dense, il n'est pas possible de référencer convenablement toutes les initiatives ouvertes et coopératives qui émergent, en revanche un certain nombre d'organisations sensibles à ces sujets ont entamé un travail de référencement que vous retrouverez ci-après.

Liste des ressources :
- [inno³ - Veille COVID-19 : Pour de l’open en conscience](https://inno3.fr/actualite/veille-covid-19-pour-de-lopen-en-conscience)
- [Open Data France - Le COVID19 et les données](http://www.opendatafrance.net/2020/03/23/le-covid19-et-les-donnees/)
- [IRD - Covid-19 : l'expertise Sud et le partenariat de l'IRD contre la pandémie](https://www.ird.fr/covid19)
- [Réseau Français des FabLabs - Coronavirus](http://www.fablab.fr/coronavirus/)
- [naos - COVID-19 : Biens communs](https://naos-cluster.com/covid-19-biens-communs/)
- [COVID19: que lire?](https://www.covid19-que-lire.fr/)
- [OSPI - COVID-19 Innovative Response Tracker](https://oecd-opsi.org/covid-response/?_countries=france&_search=france)
- [Codes sources du secteur public - COVID-19](https://code.etalab.gouv.fr/fr/repos?q=covid)

Pour ajouter une autre référence, envoyez un mail à covid19-open-memo@framalistes.org 
